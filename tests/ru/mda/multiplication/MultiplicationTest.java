package ru.mda.multiplication;

import org.junit.Test;

import static org.junit.Assert.*;

public class MultiplicationTest {

    @Test
    public void multirlyZero() {
        int multiplierOne = 0;
        int multiplierTwo = 0;
        int result=Multiplication.multiply(multiplierOne, multiplierTwo);
        assertEquals(0, result);
    }
    @Test
    public void multirlyNegative() {
        int multiplierOne = -1 + (int) (Math.random()*-100);
        int multiplierTwo = -1 + (int) (Math.random()*-100);
        int result=Multiplication.multiply(multiplierOne, multiplierTwo);
        assertEquals(multiplierOne * multiplierTwo, result);
    }
    @Test
    public void multirlyParityFirstNumber(){
        int multiplierOne = 1 + (int) (Math.random() * 100);
        int multiplierTwo = 1 + (int) (Math.random() * 100);
        if(multiplierOne%2!=0){
            multiplierOne--;
        }
        if(multiplierTwo%2==0){
            multiplierTwo++;
        }
        int result=Multiplication.multiply(multiplierOne, multiplierTwo);
        assertEquals(multiplierOne*multiplierTwo,result);
    }
    @Test
    public void multirlyParitySecondNumber(){
        int multiplierOne = 1 + (int) (Math.random() * 100);
        int multiplierTwo = 1 + (int) (Math.random() * 100);
        if(multiplierTwo%2!=0){
            multiplierTwo--;
        }
        if(multiplierOne%2==0){
            multiplierOne++;
        }
        int result=Multiplication.multiply(multiplierOne, multiplierTwo);
        assertEquals(multiplierOne*multiplierTwo,result);
    }
    @Test
    public void multirlyParityFirstNumberAndNegative(){
        int multiplierOne = -1 + (int) (Math.random() * -100);
        int multiplierTwo = 1 + (int) (Math.random() * 100);
        if(multiplierOne%2!=0){
            multiplierOne--;
        }
        if(multiplierTwo%2==0){
            multiplierTwo++;
        }
        int result=Multiplication.multiply(multiplierOne, multiplierTwo);
        assertEquals(multiplierOne*multiplierTwo,result);
    }
    @Test
    public void multirlyParitySecondNumberAndNegative(){
        int multiplierOne = 1 + (int) (Math.random() * 10);
        int multiplierTwo = -1 + (int) (Math.random() * -10);
        if(multiplierTwo%2!=0){
            multiplierTwo--;
        }
        if(multiplierOne%2==0){
            multiplierOne++;
        }
        int result=Multiplication.multiply(multiplierOne, multiplierTwo);
        assertEquals(multiplierOne*multiplierTwo,result);
    }
}
