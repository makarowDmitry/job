package ru.mda.mathInteger;

import org.junit.Test;
import ru.mda.mathInteger.InputConsole.Main;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void dataCheckingTrue() {
        String example = "5 + 8";
        boolean chec = Main.dataChecking(example);
        assertEquals(true, chec);
    }

    @Test
    public void dataCheckingFalse() {
        String example = "5+8";
        boolean chec = Main.dataChecking(example);
        assertEquals(false, chec);
    }

    @Test
    public void dataCheckingEmptiness() {
        String example = "";
        boolean chec = Main.dataChecking(example);
        assertEquals(false, chec);
    }

    @Test
    public void dataCheckingNotСomplete() {
        String example = "5 + ";
        boolean chec = Main.dataChecking(example);
        assertEquals(false, chec);
    }
}