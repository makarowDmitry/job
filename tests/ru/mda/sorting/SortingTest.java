package ru.mda.sorting;

import org.junit.Test;

import java.util.LinkedHashMap;

import static org.junit.Assert.*;

public class SortingTest {

    @Test
    public void sortFrequency() {
        int[] arr={2, 5, 2, 6, -1, 9999999, 5, 8, 8, 8};
        LinkedHashMap<Integer, Integer> hashMap=Sorting.recordInHashmap(arr);
        int[] arrResult={8, 8, 8, 2, 2, 5, 5, 6, -1, 9999999};
        int[] arrReceived=Sorting.sortFrequency(hashMap,arr);
        assertEquals(arrResult,arrReceived);
    }
}