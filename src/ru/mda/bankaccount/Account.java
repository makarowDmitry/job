package ru.mda.bankaccount;

/**
 * Класс Account содержит поле balance, также методы попонения баланса, снятия денег с баланса, проверку баланса и проверки на исключения
 */
public class Account {
    private static long balance;

    /**
     * Конструктор без параметров
     */
    public Account() {
        this(0);
    }

    /**
     * Конструтор с параметров
     *
     * @param balance - баланс
     */
    public Account(long balance) {
        this.balance = balance;
    }

    /**
     * Метод выполняет пополнение баланса
     *
     * @param money - сумма на которую пополнят
     */
    public synchronized void putMoney(long money) {
        try {
            if (balance >= 2000) {
                wait();
            }
            negativeNumber(money);
            balance += money;
            System.out.println(balance);
            notify();
        } catch (ArithmeticException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    /**
     * Метод выполняет снятие денег с баланса
     *
     * @param money - сумма которую снимут
     */
    public synchronized void withdrawMoney(long money) {
        try {
            while (balance <= 501) {
                wait();
            }

            negativeNumber(money);
            balance -= money;
            limitBalance();
            notify();
        } catch (ArithmeticException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    /**
     * Метод проверки баланса
     *
     * @return баланс
     */
    public long getBalance() {
        return balance;
    }

    /**
     * Метод выполняет проверку данных на положительность
     *
     * @param number - проверяемые данные
     * @throws ArithmeticException - отрицательное значение проверяемых данных
     */
    private void negativeNumber(long number) throws ArithmeticException {
        if (number < 0) {
            throw new ArithmeticException("Отрицательное число");
        }
    }

    /**
     * Метод выполняет проверку баланса на наличие денег
     *
     * @throws ArithmeticException - баланс ушёл в минус
     */
    private void limitBalance() throws ArithmeticException {
        if (balance < 0) {
            throw new ArithmeticException("Баланс ушёл в минус");
        }
    }
}
