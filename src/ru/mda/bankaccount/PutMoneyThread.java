package ru.mda.bankaccount;

/**
 * Класс реализует работу потока для пополнения баланса
 */
public class PutMoneyThread extends Thread {
    private Account account;
    private long money;

    /**
     * Конструктор с параметрами
     *
     * @param account - сылка на объект
     * @param money   - количество денег для пополнения
     */
    public PutMoneyThread(Account account, long money) {
        this.account = account;
        this.money = money;
    }

    /**
     * Метод реализует работу потока
     */
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            account.putMoney(money);
        }
    }
}


