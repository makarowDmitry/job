package ru.mda.bankaccount;

/**
 * Программа эмитирует работу банковского аккаунта
 * Класс выполняет запуск 2 потоков для снятия и пополнения баланса
 *
 * @author Макаров Д.А 17ИТ18
 */
public class Start {
    public static void main(String[] args) {
        Account account = new Account(1000);
        PutMoneyThread putMoneyThread = new PutMoneyThread(account, 500);
        WithdrawMoneyThread withdrawMoneyThread = new WithdrawMoneyThread(account, 2000);
        putMoneyThread.start();
        withdrawMoneyThread.start();
        try {
            putMoneyThread.join();
            withdrawMoneyThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print(account.getBalance());
    }
}
