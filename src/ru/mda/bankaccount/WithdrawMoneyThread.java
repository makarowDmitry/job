package ru.mda.bankaccount;

/**
 * Класс реализует работу потока для снятия денег
 */
public class WithdrawMoneyThread extends Thread {
    private Account account;
    private long money;

    /**
     * Конструктор с параметрами
     *
     * @param account - сылка на объект
     * @param money - количество денег для снятия
     */
    public WithdrawMoneyThread(Account account, long money) {
        this.account = account;
        this.money = money;
    }

    /**
     * Метод реализует работу потока
     */
    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            account.withdrawMoney(money);
        }
    }
}
