package ru.mda.mathInteger;

/**
 * Класс MathInteger выполнят умножение, деление, сложение, вычитание, возведение в степень и выделение остатка
 *
 * @author Макаров Д.А 17ИТ18
 */
public class MathInteger {

    /**
     * Метод выполнеят сложение двух чисел
     *
     * @param numberOne - первое слагаемое
     * @param numberTwo - второе слагаемое
     * @return сумма
     * @throws ArithmeticException - переполнение
     */
    public static int addition(int numberOne, int numberTwo) throws ArithmeticException {
        int r = numberOne + numberTwo;
        if (((numberOne ^ r) & (numberTwo ^ r)) < 0) {
            throw new ArithmeticException("Переполнение");
        }
        int sum = numberOne + numberTwo;
        return sum;
    }

    /**
     * Метод выполняет вычитание двух чисел
     *
     * @param numberOne - уменьшаемое
     * @param numberTwo - вычитаное
     * @return разность
     * @throws ArithmeticException - переполнение
     */
    public static int subtraction(int numberOne, int numberTwo) throws ArithmeticException {
        int sub = numberOne - numberTwo;
        if (((numberOne ^ numberTwo) & (numberOne ^ sub)) < 0) {
            throw new ArithmeticException("Переполнение");

        }
        return sub;
    }

    /**
     * Метод выполняет деление двух чисел
     *
     * @param numberOne - делимое
     * @param numberTwo - делитель
     * @return частное
     * @throws ArithmeticException - деление на ноль нельзя
     */
    public static int division(int numberOne, int numberTwo) throws ArithmeticException {
        if (numberTwo == 0) {
            throw new ArithmeticException("Делить на ноль нельзя");
        }
        int quotient = numberOne / numberTwo;
        return quotient;
    }

    /**
     * Метод выполняет умножение двух чисел
     *
     * @param numberOne - первый множитель
     * @param numberTwo - второй множитель
     * @return произведение
     * @throws ArithmeticException - переполнение
     */
    public static int multiply(int numberOne, int numberTwo) throws ArithmeticException {
        if (numberOne == 0 || numberTwo == 0) {
            return 0;
        }
        long total = (long) numberOne * (long) numberTwo;
        if ((int) total != total) {
            throw new ArithmeticException("Переполение");
        }
        return (int) total;

    }

    /**
     * Метод выполняет возведение в степень
     *
     * @param numberOne - число
     * @param numberTwo - степень
     * @return число после возведения степени
     * @throws ArithmeticException - переполнение, отрицательная степень
     */
    public static int pow(int numberOne, int numberTwo) throws ArithmeticException {
        if (numberOne == 0 && numberTwo == 0) {
            return 1;
        }
        if (numberOne == 0) {
            return 0;
        }
        if (numberTwo == 0) {
            return 1;
        }
        if (numberTwo < 0) {
            throw new ArithmeticException("Возведение в отрицательную степень запрещено");
        }
        long total = 1;

        for (int i = 0; i < numberTwo; i++) {
            total = total * numberOne;
        }
        if ((int) total != total) {
            throw new ArithmeticException("Переполнение");
        }
        return (int) total;
    }

    /**
     * Метод residue выделяет остаток
     *
     * @param numberOne - делимое
     * @param numberTwo - делитель
     * @return остаток
     * @throws ArithmeticException - делить на ноль нельзя
     */
    public static int residue(int numberOne, int numberTwo) {
        if (numberOne == 0) {
            return 0;
        }
        if (numberTwo == 0) {
            throw new ArithmeticException("Делить на ноль нельзя");
        }
        int total = numberOne % numberTwo;
        return total;
    }
}
