package ru.mda.mathInteger.InputConsole;

import ru.mda.mathInteger.MathInteger;

import java.util.Scanner;

/**
 * Класс Main обрабатывает вводимые данные
 *
 * @author Макаров Д.А.
 */
public class Main {
    public static void main(String[] args) {
        String example = input();
        if (dataChecking(example)) {
            System.out.print(calculator(parseNumberOne(split(example)), parseNumberTwo(split(example)), charAtSign(split(example))));
        } else {
            System.out.print("Неверный ввод данных");
        }

    }

    /**
     * Метод input производит ввод даннных
     *
     * @return строку введённую пользователем
     */
    private static String input() {
        Scanner scanner = new Scanner(System.in);
        String example = scanner.nextLine();
        return example;
    }

    /**
     * Метод dataChecking проверяет соответсвует ли введённая строка норме
     *
     * @param example введённая строка
     * @return true или false
     */
    public static boolean dataChecking(String example) {
        boolean checking = false;
        if (example.matches("[+-]?[0-9]+\\s[+-/*^%]?\\s[+-]?[0-9]+")) {
            checking = true;
        }
        return checking;
    }

    /**
     * Метод split делит string на массив
     *
     * @param example - пример
     * @return массив string
     */
    private static String[] split(String example) {
        String[] arr;
        String delimetr = " ";
        arr = example.split(delimetr);
        return arr;
    }

    /**
     * Метод parseNumberOne преобразует первый элемент массива в число
     *
     * @param arr - массив string
     * @return число преобразованное из массива
     */
    private static int parseNumberOne(String[] arr) {
        int numberOne=0;
        try {
            numberOne = Integer.parseInt(arr[0]);
        }catch (NumberFormatException e){
            e.printStackTrace();
        }
        return numberOne;
    }

    /**
     * Метод parseNumberTwo преобразует третий элемент массива в число
     *
     * @param arr - массив string
     * @return число преобразованное из массива
     */
    private static int parseNumberTwo(String[] arr) {
        int numberTwo=0;
        try {
            numberTwo = Integer.parseInt(arr[2]);
        }catch (NumberFormatException e){
            e.printStackTrace();
        }
        return numberTwo;
    }

    /**
     * Метод charAtSign преобразует второй элемент массива в char
     *
     * @param arr - массив string
     * @return - знак в виде char
     */
    private static char charAtSign(String[] arr) {
        char sign = arr[1].charAt(0);
        return sign;
    }

    /**
     * Метод calculator вызывает арифметические методы из класса MathInteger
     *
     * @param numberOne - первое число в примере
     * @param numberTwo - второе число в примере
     * @param sign      - знак
     * @return результат примера
     */
    private static int calculator(int numberOne, int numberTwo, char sign) {
        int result = 0;
        try {
            switch (sign) {
                case '+':
                    result = MathInteger.addition(numberOne, numberTwo);
                    break;
                case '-':
                    result = MathInteger.subtraction(numberOne, numberTwo);
                    break;
                case '/':
                    result = MathInteger.division(numberOne, numberTwo);
                    break;
                case '*':
                    result = MathInteger.multiply(numberOne, numberTwo);
                    break;
                case '^':
                    result = MathInteger.pow(numberOne, numberTwo);
                    break;
                case '%':
                    result = MathInteger.residue(numberOne, numberTwo);
            }
        } catch (ArithmeticException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }


}

