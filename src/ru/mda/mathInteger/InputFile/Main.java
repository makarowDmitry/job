package ru.mda.mathInteger.InputFile;

import ru.mda.mathInteger.MathInteger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс Main обрабатыватыет вводимые данные
 *
 * @author Макаров Д.А.
 */
public class Main {
    public static void main(String[] args) {
        String way = "src//ru//mda//mathInteger//file//file.txt";
        output(launch(inputFile(way)));
    }

    /**
     * Метод inputFile считывает данные с файла
     *
     * @param way - путь к файлу
     * @return arraylist который содержит ответы на примеры
     */
    private static ArrayList inputFile(String way) {
        ArrayList arrayList = new ArrayList();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(way))) {
            String read;
            while ((read = bufferedReader.readLine()) != null) {
                arrayList.add(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    /**
     * Метод осуществляет вызов других методов для перезаписи arraylist
     *
     * @param arrayList - примеры из файла
     * @return - ответы на примеры
     */
    private static ArrayList launch(ArrayList arrayList){
        for (int i = 0; i <arrayList.size() ; i++) {
            if (dataChecking(arrayList.get(i).toString())) {
                arrayList.set(i,calculator(parseNumberOne(split(arrayList.get(i).toString())), parseNumberTwo(split(arrayList.get(i).toString())), charAtSign(split(arrayList.get(i).toString()))));
            } else {
                String read = "Неверный ввод";
                arrayList.set(i,read);
            }
        }
        return arrayList;
    }

    /**
     * Метод output производит запись данных в файл
     *
     * @param arrayList - значения решённых примеров
     */
    private static void output(ArrayList arrayList) {
        String way = "src//ru//mda//mathInteger//file//output.txt";
        String number;
        try (FileWriter writer = new FileWriter(way, false)) {
            for (int i = 0; i < arrayList.size(); i++) {
                number = arrayList.get(i).toString();
                writer.append(number);
                writer.append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Метод dataChecking проверяет соответсвует ли введённая строка норме
     *
     * @param example введённая строка
     * @return true или false
     */
    public static boolean dataChecking(String example) {
        boolean checking = false;
        if (example.matches("[+-]?[0-9]+\\s[+-/*^%]?\\s[+-]?[0-9]+")) {
            checking = true;
        }
        return checking;
    }

    /**
     * Метод split делит string на массив
     *
     * @param example - пример
     * @return массив string
     */
    private static String[] split(String example) {
        String[] arr;
        String delimetr = " ";
        arr = example.split(delimetr);
        return arr;
    }

    /**
     * Метод parseNumberOne преобразует первый элемент массива в число
     *
     * @param arr - массив string
     * @return число преобразованное из массива
     */
    private static int parseNumberOne(String[] arr) {
        int numberOne = 0;
        try {
            numberOne = Integer.parseInt(arr[0]);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return numberOne;
    }

    /**
     * Метод parseNumberTwo преобразует третий элемент массива в число
     *
     * @param arr - массив string
     * @return число преобразованное из массива
     */
    private static int parseNumberTwo(String[] arr) {
        int numberTwo = 0;
        try {
            numberTwo = Integer.parseInt(arr[2]);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return numberTwo;
    }

    /**
     * Метод charAtSign преобразует второй элемент массива в char
     *
     * @param arr - массив string
     * @return - знак в виде char
     */
    private static char charAtSign(String[] arr) {
        char sign = arr[1].charAt(0);
        return sign;
    }

    /**
     * Метод calculator вызывает арифметические методы из класса MathInteger
     *
     * @param numberOne - первое число в примере
     * @param numberTwo - второе число в примере
     * @param sign      - знак
     * @return результат примера
     */
    private static int calculator(int numberOne, int numberTwo, char sign) {
        int result = 0;
        try {
            switch (sign) {
                case '+':
                    result = MathInteger.addition(numberOne, numberTwo);
                    break;
                case '-':
                    result = MathInteger.subtraction(numberOne, numberTwo);
                    break;
                case '/':
                    result = MathInteger.division(numberOne, numberTwo);
                    break;
                case '*':
                    result = MathInteger.multiply(numberOne, numberTwo);
                    break;
                case '^':
                    result = MathInteger.pow(numberOne, numberTwo);
                    break;
                case '%':
                    result = MathInteger.residue(numberOne, numberTwo);
            }
        } catch (ArithmeticException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }


}

