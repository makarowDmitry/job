package ru.mda.chocolate;

import java.util.Scanner;

/**
 * Класс Chocolate производит подсчёт возможно купленных шоколадок
 *
 * @author Макаров Д.А 17ИТ18
 */
public class Chocolate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите сколько у вас денег");
        int money = scanner.nextInt();
        System.out.println("Введите цену за шоколадку");
        int price = scanner.nextInt();
        System.out.println("Введите количество обёрток, нужное, чтобы получить ещё одну шоколадку");
        int wrap = scanner.nextInt();
        System.out.println(Chocolate.count(money, price, wrap));
    }

    /**
     * @param money - количество денег
     * @param price - цена за 1 шоколадку
     * @param wrap  - количество обвёрток нужное для обмена на 1 шоколадку
     * @return
     */
    private static int count(int money, int price, int wrap) {
        int chocolatesBeningMoney = 0;
        int chocolates = 0;
        chocolatesBeningMoney = money / price;
        int chocolatesBeningWraper = chocolatesBeningMoney / wrap;
        money = chocolatesBeningWraper;
        if (money > 0) {
            chocolates = chocolatesBeningMoney + count(money, price, wrap);
        }
        return chocolates;
    }
}
