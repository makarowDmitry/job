package ru.mda.jeans;

import java.util.Scanner;

public class Jeans {
    public static void main(String[]args){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Введите сколько у вас денег");
        double money=scanner.nextDouble();
        if(money<0){
            System.out.println("Значение денег не может быть меньше нуля, введите корректное число");
            money=scanner.nextDouble();
        }
        System.out.println("Введите какая сейчас скидка на джинсы");
        double sale=scanner.nextDouble();
        if(sale<1&&sale>100){
            System.out.println("Скидка е может быть меньше 1 или больше 100, введите корректное число");
            sale=scanner.nextDouble();
        }
        System.out.println("Введите сколько стоят джинсы");
        double prise=scanner.nextDouble();
        if(prise<0){
            System.out.println("Значение цены не может быть меньше нуля, введите корректное число");
            prise=scanner.nextDouble();
        }
        Jeans.shoping(money,sale,prise);
    }
    private static void shoping(double money,double sale, double prise){
        double priseFromSale=prise-(prise/100*sale);
        if(money>priseFromSale){
            System.out.println("Вам хватит денег");
        }else {
            System.out.println("Вам не хватит денег");
        }
    }
}
