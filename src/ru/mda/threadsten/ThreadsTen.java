package ru.mda.threadsten;

/**
 * Класс ThreadsTen выполняет запуск 10 потоков
 *
 * @author Макаров Д.А 17ИТ18
 */
public class ThreadsTen extends Thread {
    private String name;

    private ThreadsTen(String name) {
        this.name = name;
        setName(name);
    }

    public void run() {
        System.out.println(name);
    }

    public static void main(String[] args) {
        for (int i = 0; i <= 10; i++) {
            Thread thread = new ThreadsTen("Номер потока " + i);
            thread.start();
        }
        System.out.println("Главный поток завершился");

    }
}



