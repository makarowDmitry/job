package ru.mda.copyfiles;
/**
 * Класс выполняет параллельное копирование двух файлов nio
 *
 * @author Макаров Д.А 17ИТ18
 */
public class CopyFilesNIOThreads implements Constants {
    public static void main(String[] args) {
        long before=System.currentTimeMillis();
        Thread threadOne=new ThreadsNIO(PATH_INPUT_ONE,PATH_OUTPUT_ONE);
        Thread threadTwo=new ThreadsNIO(PATH_INPUT_TWO,PATH_OUTPUT_TWO);
        threadOne.start();
        threadTwo.start();
        try {
            threadOne.join();
            threadTwo.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        long after=System.currentTimeMillis();
        System.out.println("Время выполнения главного потока: "+(after-before));
    }
}
