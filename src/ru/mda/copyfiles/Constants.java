package ru.mda.copyfiles;

public interface Constants {
    final String PATH_INPUT_ONE = "src//ru//mda//copyfiles//files//inputOne.txt";
    final String PATH_INPUT_TWO = "src//ru//mda//copyfiles//files//inputTwo.txt";
    final String PATH_OUTPUT_ONE = "src//ru//mda//copyfiles//files//outputOne.txt";
    final String PATH_OUTPUT_TWO = "src//ru//mda//copyfiles//files//outputTwo.txt";
}
