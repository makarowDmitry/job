package ru.mda.copyfiles;
/**
 * Класс выполняет параллельное копирование двух файлов
 *
 * @author Макаров Д.А 17ИТ18
 */
public class CopyFilesThreads implements Constants {
    public static void main(String[] args) {
        Thread threadOne = new Threads(PATH_INPUT_ONE, PATH_OUTPUT_ONE);
        Thread threadTwo = new Threads(PATH_INPUT_TWO, PATH_OUTPUT_TWO);
        long before = System.currentTimeMillis();
        threadOne.start();
        threadTwo.start();
        try {
            threadOne.join();
            threadTwo.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long after = System.currentTimeMillis();
        System.out.println("Время главного потока: " + (after - before));
    }
}
