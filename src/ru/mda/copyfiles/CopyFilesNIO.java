package ru.mda.copyfiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Класс CopyFiles выполняет последовательно копирование двух файлов с помощью nio
 *
 * @author Макаров Д.А 17ИТ18
 */
public class CopyFilesNIO implements Constants {
    public static void main(String[] args) {
        long before=System.currentTimeMillis();
        Path sourseOne= Paths.get(PATH_INPUT_ONE);
        Path targetOne=Paths.get(PATH_OUTPUT_ONE);
        Path sourseTwo= Paths.get(PATH_INPUT_TWO);
        Path targetTwo= Paths.get(PATH_OUTPUT_TWO);
        try {
            Files.copy(sourseOne, targetOne, StandardCopyOption.REPLACE_EXISTING);
            Files.copy(sourseTwo, targetTwo, StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException e){
            e.printStackTrace();
        }
        long after=System.currentTimeMillis();
        System.out.println("Время выполнения: "+(after-before));
    }
}
