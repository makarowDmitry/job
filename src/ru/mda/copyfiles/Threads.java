package ru.mda.copyfiles;

import java.util.ArrayList;

public class Threads extends Thread {
    String pathInput;
    String pathOutput;

    Threads(String pathInput, String pathOutput) {
        this.pathInput = pathInput;
        this.pathOutput = pathOutput;

    }

    public void run() {
        long before = System.currentTimeMillis();
        ArrayList arraylist = new ArrayList();
        CopyFiles.inputFile(pathInput, arraylist);
        CopyFiles.outputFile(pathOutput, arraylist);
        long after = System.currentTimeMillis();
        System.out.println("Время потока " + getName() + ": " + (after - before));
    }

}
