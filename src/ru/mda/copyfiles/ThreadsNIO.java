package ru.mda.copyfiles;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class ThreadsNIO extends Thread {
    String pathInput;
    String pathOutput;

    ThreadsNIO(String pathInput, String pathOutput) {
        this.pathInput = pathInput;
        this.pathOutput = pathOutput;

    }

    public void run(){
        long before=System.currentTimeMillis();
        Path sourse = Paths.get(pathInput);
        Path recipient = Paths.get(pathOutput);
        try {
            Files.copy(sourse, recipient, StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException e){
            e.printStackTrace();
        }
        long after=System.currentTimeMillis();
        System.out.println("Время выполнения "+getName()+": "+(after-before));
    }
}
