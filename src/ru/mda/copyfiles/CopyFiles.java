package ru.mda.copyfiles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Класс CopyFiles выполняет последовательно копирование двух файлов
 *
 * @author Макаров Д.А 17ИТ18
 */
public class CopyFiles implements Constants {
    public static void main(String[] args) {
        long before = System.currentTimeMillis();
        copy();
        long after = System.currentTimeMillis();
        System.out.println(after - before + " миллисекунд");
    }

    /**
     * Метод выполняет запуск методов для копирования данных из одних файлов в другие
     */
    private static void copy() {
        ArrayList arrayList = new ArrayList();
        inputFile(PATH_INPUT_ONE, arrayList);
        outputFile(PATH_OUTPUT_ONE, arrayList);
        cleaningUp(arrayList);
        inputFile(PATH_INPUT_TWO, arrayList);
        outputFile(PATH_OUTPUT_TWO, arrayList);
    }

    /**
     * Метод считывает данные из файла в массив ArrayList
     *
     * @param path - путь к файлу
     * @param arrayList - массив в который будут записывать данные из файла
     * @return массив с данными из файла
     */
    public static ArrayList inputFile(String path, ArrayList arrayList) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            String read;
            while ((read = bufferedReader.readLine()) != null) {
                arrayList.add(read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    /**
     * Метод записывает данных в файл
     *
     * @param path - путь к файлу
     * @param arrayList массив из которого берутся данные для записи в файл
     */
    public static void outputFile(String path, ArrayList arrayList) {
        String number;
        try (FileWriter writer = new FileWriter(path, false)) {
            for (int i = 0; i < arrayList.size(); i++) {
                number = arrayList.get(i).toString();
                writer.append(number);
                writer.append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод удаляет из массива все данные
     *
     * @param arrayList массив данных
     * @return пустой массив
     */
    public static ArrayList cleaningUp(ArrayList arrayList) {
        arrayList.clear();
        return arrayList;
    }

}
