package ru.mda.eggorchicken;

/**
 * Класс Main выполняет запуск двух потоков и проверят какой закончится первым
 *
 * @author Макаров Д.А
 */
public class Main {
    public static void main(String[] args) {
        Thread egg = new EggOrChicken("egg");
        Thread chicken = new EggOrChicken("chicken");
        chicken.start();
        egg.start();
        try {
            egg.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        if(chicken.isAlive()){
            System.out.println("Яйцо было первое");
        }else {
            System.out.println("Курица была первой");
        }
    }
}
