package ru.mda.lossofresult;

public class Threads  extends Thread {
    private final Example checker;
    private static int i;

    Threads(Example checker){
        this.checker=checker;
    }
    private synchronized static void increment(){
        i++;
    }

    int getI(){
        return i;
    }

    public void run(){
         while (!checker.stop()){
            increment();
        }
    }

}
