package ru.mda.lossofresult;

import java.util.concurrent.atomic.AtomicInteger;

public class Example {
    private static final int TEN_MILLION = 10000000;
    private AtomicInteger counter = new AtomicInteger();

    boolean stop() {
        return counter.incrementAndGet() > TEN_MILLION;
    }

    void exampl() throws InterruptedException {
        Threads thread1 = new Threads(this);
        Threads thread2 = new Threads(this);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("Ожидаем: "+TEN_MILLION);
        System.out.println("Результат: "+thread1.getI());
    }
}
