package ru.mda.withoutlossofresult;


import java.util.concurrent.atomic.AtomicInteger;

public class Example2 {
    private static final int TEN_MILLION = 10000000;
    private AtomicInteger counter = new AtomicInteger();
    private Object object=new Object();

    boolean stop() {
        return counter.incrementAndGet() > TEN_MILLION;
    }

    void exampl() throws InterruptedException {
        ThreadsIncrement thread1= new ThreadsIncrement(this,object);
        ThreadsIncrement thread2= new ThreadsIncrement(this,object);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("Ожидаем: "+TEN_MILLION);
        System.out.println("Результат: "+object.getI());
    }
}
