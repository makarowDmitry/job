package ru.mda.withoutlossofresult;

public class Object {
    private int i;

    synchronized void increment(){
        i++;
    }

    public  int getI(){
        return i;
    }
}
