package ru.mda.withoutlossofresult;

public class ThreadsIncrement extends Thread {
    private final Example2 checker;
    private final Object object;

    ThreadsIncrement(Example2 checker, Object object){
        this.checker=checker;
        this.object=object;
    }

    public void run(){
         while (!checker.stop()){
            object.increment();
        }
    }

}
