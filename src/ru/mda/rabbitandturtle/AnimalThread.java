package ru.mda.rabbitandturtle;

public class AnimalThread extends Thread {
    String name;
    int priority;

    public AnimalThread(String name, int priority){
        this.name=name;
        this.priority=priority;
        setName(name);
        setPriority(priority);
    }
    public void run(){
        for (int i = 0; i <200 ; i++) {
            System.out.println(getName()+": "+i);
            if(getName()=="rabbit"&& i>50){
                setPriority(1);
            }
            if(getName()=="turtle"&& i>50){
                setPriority(10);
            }
        }
    }
}
