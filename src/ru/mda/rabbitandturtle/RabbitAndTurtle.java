package ru.mda.rabbitandturtle;

public class RabbitAndTurtle {
    public static void main(String[] args) {
        Thread rabbit = new AnimalThread("rabbit", 5);
        Thread turtle = new AnimalThread("turtle", 1);
        turtle.start();
        rabbit.start();

    }
}
