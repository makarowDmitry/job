package ru.mda.sorting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;

/**
 * Класс Sorting выполняет сортировку по частоте
 *
 * @author Макаров Д.А. 17ИТ18
 */
public class Sorting {
    public static void main(String[] args) {
        int[] arr = {2, 5, 2, 6, -1, 9999999, 5, 8, 8, 8};
        print(arr);
        sortFrequency(Sorting.recordInHashmap(arr), arr);
        print(arr);
    }

    /**
     * Метод записывает массив в список linkedhashmap (ключ(число),значение(частота повторений))
     *
     * @param arr - исходный массив
     * @return hashmap
     */
    public static LinkedHashMap<Integer, Integer> recordInHashmap(int arr[]) {
        LinkedHashMap<Integer, Integer> hashMap = new LinkedHashMap<>();
        int frequency;
        for (int i = 0; i < arr.length; i++) {
            frequency = 0;
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    frequency++;
                }
            }
            int key = arr[i];
            if (!hashMap.containsKey(key)) {
                hashMap.put(key, frequency);
            }
        }
        return hashMap;
    }

    /**
     * Метод производит сортировку по частоте
     *
     * @param hashMap - linkedhashmap
     * @param arr     - исходный массив
     */
    public static int[] sortFrequency(LinkedHashMap<Integer, Integer> hashMap, int arr[]) {
        ArrayList<Integer> arrayKey = new ArrayList<>(hashMap.keySet());
        ArrayList<Integer> arrayValue = new ArrayList<>(hashMap.values());
        int maxNumber = Collections.max(arrayValue);
        int arrIndex = 0;
        for (int i = 0; i < arrayValue.size(); i++) {
            if (arrayValue.get(i) == maxNumber) {
                for (int j = 0; j < maxNumber; j++) {
                    arr[arrIndex] = arrayKey.get(i);
                    arrIndex++;
                }
                arrayKey.remove(i);
                arrayValue.remove(i);
                maxNumber = Collections.max(arrayValue);
                i = -1;
            }
            //Если в массиве arrayValue остаётся один элемент, то он записывается в массив arr
            if (arrayValue.size() == 1 && arrayValue.get(0) != 0) {
                arr[arrIndex] = arrayKey.get(0);
                arrIndex++;
                arrayValue.set(0, arrayValue.get(0) - 1);
            }
        }
        return arr;
    }

    /**
     * Метод выводит в консоль массив
     *
     * @param arr - отсортированный по частоте массив
     */
    private static void print(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println(" ");
    }
}

