package ru.mda.multiplication;

import java.util.Scanner;

/**
 * Класс Multiplication выводит произведение двух множителей
 *
 * @author Макаров Д.А. 17ИТ18
 */
public class Multiplication {
    public static void main(String[] args) {
        Scanner add = new Scanner(System.in);
        System.out.println("Введите первое число");
        int multiplierOne = add.nextInt();
        System.out.println("Введите второе число");
        int multiplierTwo = add.nextInt();
        System.out.println(Multiplication.multiply(multiplierOne, multiplierTwo));
    }

    /**
     * Производит умножение параметров
     *
     * @param multiplierOne - первый множитель
     * @param multiplierTwo - второй множитель
     * @return произведение первого и второго множителя
     */
    public static int multiply(int multiplierOne, int multiplierTwo) {
        if (multiplierOne == 0 || multiplierTwo == 0) {
            return 0;
        }
        int negativeNumbers = 0;
        if (multiplierOne < 0) {
            multiplierOne = -multiplierOne;
            negativeNumbers++;
        }
        if (multiplierTwo < 0) {
            multiplierTwo = -multiplierTwo;
            negativeNumbers++;
        }

        if (multiplierOne > multiplierTwo) {
            int value = multiplierOne;
            multiplierOne = multiplierTwo;
            multiplierTwo = value;
        }
        int total = 0;
        boolean odd = false;
        if (multiplierTwo % 2 != 0 && multiplierTwo>1) {
            multiplierTwo--;
            odd = true;
        }
        multiplierTwo /= 2;
        for (int i = 0; i < multiplierTwo; i++) {
            total += multiplierOne + multiplierOne;
        }
        if (odd) {
            total += multiplierOne;
        }
        if (negativeNumbers == 1) {
            total = -total;
        }
        return total;
    }
}

